/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import javax.swing.JFrame;

/**
 *
 * @author allanmual
 */
public class MainGraphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Crear un formulario 
        JFrame frm = new JFrame("Pinball - UTN v0.1");
        //Asignar la operación de cierre del formulario, para que la aplicación
        //termine cuando se presiona la X
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Agregar el panel
        frm.add(new MiPanel());
        //Valida la interfaz
        frm.pack();
        //Centrar la ventana 
        frm.setLocationRelativeTo(null);
        //Hacemos visible la ventana 
        frm.setVisible(true);
        //
        frm.setResizable(false);
        while (true) {
            frm.repaint();
            try {
                Thread.sleep(15); // FPS
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
