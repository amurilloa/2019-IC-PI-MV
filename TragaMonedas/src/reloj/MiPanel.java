/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel {

    private int hrs;
    private int min;
    private int seg;
    private int ms;

    public MiPanel() {
        setFocusable(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(480, 560);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        ms += 15;
        if (ms >= 1000) {
            seg++;
            ms = 0;
            if (seg >= 59) {
                min++;
                seg = 0;
                if (min >= 59) {
                    hrs++;
                    min = 0;
                    if (hrs >= 23) {
                        hrs = 0;
                    }
                }

            }
            String msj = String.format("%02d:%02d:%02d", hrs, min, seg);
            System.out.println(msj);
        }

        //Cuadricula
        g.setColor(new Color(230, 230, 231));
        int x = 0;
        int y = 0;
        for (int i = 0; i <= 21; i++) {
            g.drawLine(0, y, getWidth(), y);
            g.drawLine(x, 0, x, getHeight());
            x += 40;
            y += 40;
        }

        y = getHeight() / 2;//300
        x = getWidth() / 2;//300

        g.setColor(Color.RED);
        g.drawLine(0, y, getWidth(), y);
        g.drawLine(x, 0, x, getHeight());

    }

}
