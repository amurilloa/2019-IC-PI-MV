/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Bola {

    private int x;
    private int y;
    private int dir;
    private int spdY;
    private int spdX;
    private int tiempoY;
    private int tiempoX;
    private boolean arriba;
    private boolean hueco;

    private final Color COLOR = Color.WHITE;
    private final int SIZE = 20;

    public Bola(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.GRAY);
        g.fillOval(x + 1, y + 1, SIZE, SIZE);
        g.setColor(COLOR);
        g.fillOval(x, y, SIZE, SIZE);

//        g.setColor(Color.YELLOW);
//        g.drawRect(x + 2, y + 2, SIZE - 4, SIZE - 4);
    }

    public Rectangle getBounds() {
        return new Rectangle(x + 2, y + 2, SIZE - 4, SIZE - 4);
    }

    public void iniciar(int ms) {
        dir = 2;
        int cal = ms / 100;
        spdY = cal > 10 ? 10 : cal;
    }

    private boolean chocoHueco(LinkedList<Hueco> huecos){
        for (Hueco h : huecos) {
            if(getBounds().intersects(h.getBounds()) && !h.isBola()){
                x = h.getX()+2;
                y = h.getY()+2;
                h.setBola(true);
                return true;
            }
        }
        return false;
    }
    
    public void mover(LinkedList<Hueco> huecos) {
        if (dir != 0 && !hueco) {
            tiempoY++;
            tiempoX++;
            y -= spdY;
            x -= spdX;
            if(chocoHueco(huecos)){
                dir = 0;
                hueco = true;
                return;
            }

            if (tiempoY >= 77) {
                spdY = spdY == 1 ? -1 : spdY - 1;
                tiempoY = 0;
            }

            if (tiempoX >= 120 && arriba) {
                spdX = spdX == 1 ? 1 : spdX - 1;
                tiempoX = 0;
            }

            if (y <= 100 && !arriba) {
                arriba = true;
                spdX = spdY > 3 ? 2 : 1;
                spdY = 1;
            }

            //Truco
            if (y >= 480) {
                dir = 0;
                y = 480;
                x = 450;
                spdX = 0;
                arriba = false;
            }
        }
    }

    public boolean isHueco() {
        return hueco;
    }

    public void setHueco(boolean enHueco) {
        this.hueco = enHueco;
    }

}
