/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author allanmual
 */
public class Fondo {

    public void paint(Graphics2D g) {
        g.setColor(new Color(240,240,240));
        g.fillRect(0,0,480,560);
        g.setColor(new Color(225,225,225));
        g.fillRect(0, 0, 560, 120);
        g.setColor(new Color(240,240,240));
        g.fillOval(-50,0, 580, 300);
    }

}
