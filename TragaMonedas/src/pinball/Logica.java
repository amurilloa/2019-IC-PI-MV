/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Logica {

    private final int ancho;
    private final int alto;
    //private final Bola bolita;
    private final LinkedList<Bola> bolitas;
    private final LinkedList<Hueco> huecos;
    private final Tirador tirador;
    private final Fondo fondo;

    public Logica(int ancho, int alto) {
        this.ancho = ancho;
        this.alto = alto;
        bolitas = new LinkedList<>();
        huecos = new LinkedList<>();
        bolitas.add(new Bola(450, 480)); //Prueba
        colocarHuecos();
        tirador = new Tirador();
        fondo = new Fondo();
    }

    private void colocarHuecos() {
        int inicial = 68;
        int x = inicial;
        int y = 128;

        for (int f = 3; f >= 0; f--) {
            inicial = x;
            for (int h = 0; h < f + 2; h++) {
                huecos.add(new Hueco(x, y)); //Prueba
                x += 80;
            }
            y += 80;
            x = inicial + 40;
        }
    }

    public void run(Graphics2D g) {
        fondo.paint(g);
        if (tirador.isPresionado()) {
            tirador.inc(15);
        }

        for (Hueco hueco : huecos) {
            hueco.paint(g);
        }

        for (Bola bolita : bolitas) {
            bolita.paint(g);
        }

        bolitas.getLast().mover(huecos);

        if (bolitas.getLast().isHueco() && bolitas.size() < 6) {
            bolitas.add(new Bola(450, 480));
        }
    }

    public void accionesPre(int tecla) {
        if (tecla == KeyEvent.VK_DOWN) {
            tirador.setPresionado(true);
        }
    }

    public void accionesRel(int tecla) {
        if (tecla == KeyEvent.VK_DOWN) {
            tirador.setPresionado(false);
            System.out.println(tirador.getMs());
            bolitas.getLast().iniciar(tirador.getMs());
            tirador.setMs(0);
        }

    }

}
