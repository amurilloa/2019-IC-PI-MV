/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel implements KeyListener {

    private Logica log;

    public MiPanel() {
        setFocusable(true);
        addKeyListener(this);
        log = new Logica(480, 560);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(480, 560);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        log.run(g);
        
        //Cuadricula
        g.setColor(new Color(230, 230, 231));
        int x = 0;
        int y = 0;
        for (int i = 0; i <= 21; i++) {
            g.drawLine(0, y, getWidth(), y);
            g.drawLine(x, 0, x, getHeight());
            x += 40;
            y += 40;
        }

        y = getHeight() / 2;//300
        x = getWidth() / 2;//300

        g.setColor(Color.RED);
        g.drawLine(0, y, getWidth(), y);
        g.drawLine(x, 0, x, getHeight());

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        log.accionesPre(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        log.accionesRel(e.getKeyCode());
    }

}
