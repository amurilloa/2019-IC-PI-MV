/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 *
 * @author allanmual
 */
public class Hueco {

    private final int x;
    private final int y;
    private boolean bola;
    private final int SIZE = 24;
    

    public Hueco(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.fillOval(x, y, SIZE, SIZE);
        g.setColor(Color.WHITE);
        g.fillOval(x + 1, y + 1, SIZE - 2, SIZE - 2);
        g.setColor(Color.BLACK);
        g.fillOval(x + 3, y + 3, SIZE - 6, SIZE - 6);

//        g.setColor(Color.YELLOW);
//        g.drawRect(x + 7, y + 7, SIZE - 14, SIZE - 14);
    }

    public Rectangle getBounds() {
        return new Rectangle(x + 7, y + 7, SIZE - 14, SIZE - 14);
    }

    public boolean isBola() {
        return bola;
    }

    public void setBola(boolean bola) {
        this.bola = bola;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    
}
