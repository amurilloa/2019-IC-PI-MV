/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplogarrobos;

/** 
 *
 * @author allanmual
 */
public class MainClases {

    public static void main(String[] args) {
        //Lectura recomendada: Capitulo 3: Como programar en Java.
        Carro c1 = new Carro("HBX-692", "Toyota", "Yaris", 2008, 'A', 4000000);
        Carro c2 = new Carro("BBX-432", "Nissan", "Tida", 2019, 'A', 15000000);
        Carro c3 = new Carro();
        c3.setPlaca("1322123");
        
        System.out.println(c1.getPlaca() + c1.getMarca() + c1.getModelo());
        System.out.println(c2.getPlaca() + c2.getMarca() + c2.getModelo());
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        
        // Hacer dos clases, con minimo 6 atributos, 2 constructores(sin contar
        // el vacio), get y set, toString
        
    }
}
