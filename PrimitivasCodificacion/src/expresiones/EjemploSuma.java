/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresiones;

import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class EjemploSuma {

    public static void main(String[] args) { // La interfaz del usuario: interactuar con la app

        Logica log = new Logica();
        System.out.println("Ejercicio 1");
        int a = 5;//Integer.parseInt(JOptionPane.showInputDialog("#1"));
        int b = 6;//Integer.parseInt(JOptionPane.showInputDialog("#2"));
        int t = log.suma(a, b);
        System.out.println(a + "+" + b + "=" + t);

        System.out.println("\nEjercicio 2");
        String msj = log.mayor(a, b);
        System.out.println(msj);

        System.out.println("\nEjercicio 3");
        double n = 5.0;//Double.parseDouble(JOptionPane.showInputDialog("Digite un número"));
        System.out.println(log.negPos(n));

        System.out.println(log.esPar(a));
        System.out.println(log.esPar(b));
        System.out.println(log.esPar(t));

        String msj2 = log.esPar(b) ? "es par" : "es impar";
        System.out.println(msj2);

        int x = 10;//Integer.parseInt(JOptionPane.showInputDialog("Digite el valor para x"));
        int y = 11;//Integer.parseInt(JOptionPane.showInputDialog("Digite el valor para y"));
        System.out.println(log.compararNum(x, y));

        int ale = log.numAle(1, 10);
        System.out.println(ale);

        int costo = 10000;//Integer.parseInt(JOptionPane.showInputDialog("Costo: "));
        int ejes = 2; //Integer.parseInt(JOptionPane.showInputDialog("Ejes: "));
        int pasa = 5;//Integer.parseInt(JOptionPane.showInputDialog("Pasajeros: "));
        System.out.println("Costo Final: " + log.costoFinal(costo, pasa, ejes));

        int nota = 75;
        System.out.println(log.notas(nota));

        int par = 5;//Integer.parseInt(JOptionPane.showInputDialog("Número de Partituras"));
        int can = 10;//Integer.parseInt(JOptionPane.showInputDialog("Número de Canciones"));
        System.out.println(log.tipoMusico(can, par));

        System.out.println(log.monedas(888));

        int valor = Integer.parseInt(JOptionPane.showInputDialog("Digite el valor del crucero"));
        int personas = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de personas"));
        int edad = personas == 1 ? Integer.parseInt(JOptionPane.showInputDialog("Digite la edad del viajero")) : 0;

//        if (personas == 1) {
//            edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad del viajero"));
//        } else {
//            edad = 0;
//        }
        System.out.println(log.costoCrucero(valor, personas, edad));
    }
}
