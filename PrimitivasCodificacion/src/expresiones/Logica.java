/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresiones;

/**
 *
 * @author allanmual
 */
public class Logica {

    public int suma(int n1, int n2) {
        int total = n1 + n2;
        return total;
    }

    public String mayor(int n1, int n2) {

        if (n1 > n2) {
            //Bloque de instrucciones cuando es verdadero
            return "N1 si es mayor que N2";
        } else {
            //Bloque de instrucciones cuando es falso
            return "N1 no es mayor que N2";
        }
    }

    public String negPos(double num) {

        String res = "-5 es negativo";

        if (num < 0) {
            res = num + " es negativo\n";
        } else {
            res = num + " es positivo\n";
        }

        if (num % 5 == 0) {
            res += num + " es divisible entre 5";
        }

        return res;
    }

    public boolean esPar(int num) {
        return num % 2 == 0;
    }

    public String compararNum(int x, int y) {
        if (x > y) {
            return "x > y " + "(x: " + x + ", y: " + y + ")";
        } else if (x < y) {
            String res = String.format("x < y (x: %d, y: %d)\n", x, y);
            return res;
        } else {
            return String.format("x = y (x: %d, y: %d)\n", x, y);
        }
    }

    public double costoFinal(int costo, int pasa, int ejes) {
        double impuesto = costo * 0.01; //100
        double aumEjes = 0; // 5
        double aumPasa = 0; // 1

        if (pasa < 20) {
            aumPasa = impuesto * 0.01;
        } else if (pasa >= 20 && pasa <= 60) {
            aumPasa = impuesto * 0.05;

        } else {
            aumPasa = impuesto * 0.08;
        }

        if (ejes == 2) {
            aumEjes = impuesto * 0.05;
        } else if (ejes == 3) {
            aumEjes = impuesto * 0.10;
        } else if (ejes > 3) {
            aumEjes = impuesto * 0.15;
        }
        double cf = costo + impuesto + aumEjes + aumPasa;
        return cf;
    }

    public int numAle(int min, int max) {
        return (int) (Math.random() * max) + min;
    }

    public String notas(int nota) {
        if (nota >= 0 && nota <= 100) {
            if (nota >= 90) {
                return "Sobresaliente"; // 2/1500
            } else if (nota >= 80) {
                return "Notable"; // 4/1500
            } else if (nota >= 70) {
                return "Bien"; //10/1500
            } else {
                return "Insuficiente"; //4/1500
            }
        } else {
            return "Nota inválida";
        }
    }

    public String tipoMusico(int can, int par) {
        if (can >= 7 && can <= 10 && par == 0) {
            return "Naciente";
        } else if (can >= 7 && can <= 10 && par >= 1 && par <= 5) {
            return "Estelar";
        } else if (can > 10 && par > 5) {
            return "Consagrado";
        } else {
            return "Formación";
        }
    }

    public String monedas(int monto) {
        String res = "";

        int moneda = 500;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 200;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 100;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 50;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 20;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 10;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 5;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }

        moneda = 2;
        if (monto >= moneda) {
            int mon = monto / moneda;
            //monto = monto - mon * moneda;
            monto = monto % moneda;
            res += mon + " billetes de " + moneda + "\n";
        }
        if (monto > 0) {
            res += monto + " moneda de 1";
        }
        return res;
    }

    public String costoCrucero(int valor, int personas, int edad) {
        if (personas > 0) {
            if (personas == 1) { //Viajan Solos 
                if (edad >= 18 && edad <= 30) {
                    double desc = valor * 0.078;
                    return ("Valor: " + valor)
                            + ("\nDesc.: " + desc)
                            + ("\nTotal: " + (valor - desc));
                } else if (edad > 30) {
                    double desc = valor * 0.10;
                    return ("Valor: " + valor)
                            + ("\nDesc.: " + desc)
                            + ("\nTotal: " + (valor - desc));
                } else {
                    return "Total: " + valor;
                }
            } else if (personas == 2) {
                double desc = valor * 0.115;
                return ("Valor:\t" + valor)
                        + ("\nDesc.:\t" + desc)
                        + ("\nSubTot:\t" + (valor - desc) + "pp")
                        + ("\nTotal:\t" + (valor - desc) * personas);
            } else if (personas > 3) {
                double desc = valor * 0.15;
                return ("Valor:\t" + valor)
                        + ("\nDesc.:\t" + desc)
                        + ("\nSubTot:\t" + (valor - desc) + "pp")
                        + ("\nTotal:\t" + (valor - desc) * personas);
            } else {
                return ("Valor:\t" + valor) + ("\nTotal:\t" + valor * personas);
            }
        }
        return "Número de personas inválido";
    }

   

}
