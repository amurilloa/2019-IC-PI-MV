/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class Util {

    /**
     * Permite leer un número entero, en caso de error lo solicita nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @return int valor que el usuario digito
     */
    public static int leerInt(String mensaje) {
        String msj = mensaje;
        do {
            try {
                int num = Integer.parseInt(JOptionPane.showInputDialog(msj));
                return num;
            } catch (Exception ex) {
                msj = "Intente nuevamente, " + mensaje;
            }
        } while (true);
    }

    /**
     * Permite leer un número con decimales, en caso de error lo solicita nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @return double valor que el usuario digito
     */
    public static double leerDouble(String mensaje) {
        String msj = mensaje;
        do {
            try {
                double num = Double.parseDouble(JOptionPane.showInputDialog(msj));
                return num;
            } catch (Exception ex) {
                msj = "Intente nuevamente, " + mensaje;
            }
        } while (true);
    }

    /**
     * Permite leer un texto requerido, en caso de estar vacio lo solicita nuevamente
     * @param mensaje String que le indica al usuario que debe digitar
     * @return String con el texto digitado
     */
    public static String leerTexto(String mensaje) {
        return leerTexto(mensaje, true);
    }

    /**
     * Permite leer un texto requerido o no, en caso de estar vacio y ser requerido lo solicita nuevamente
     * @param mensaje String que le indica al usuario que debe digitar
     * @param requerido True: para que el usuario tenga que digitar texto obligatoriamente
     * @return String con el texto digitado
     */
    public static String leerTexto(String mensaje, boolean requerido) {
        String msj = mensaje;
        do {
            String txt = JOptionPane.showInputDialog(msj);
            if (txt.trim().length() > 0 || !requerido) {
                return txt;
            } else {
                msj = "Datos requeridos, " + mensaje;
            }
        } while (true);
    }
    
    /**
     * Muestra un mensaje al usuario en un JOP
     * @param mensaje String que se le muestra al usuario
     */
    public static void mostrar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }
}
