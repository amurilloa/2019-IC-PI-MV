/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

/**
 *
 * @author allanmual
 */
public class Calculadora {

    /**
     * Suma dos números enteros
     *
     * @param n1 int primero número
     * @param n2 int segundo número
     * @return suma de los números anteriores
     */
    public int suma(int n1, int n2) {
        return n1 + n2;
    }

    /**
     * Resta dos números enteros
     *
     * @param n1 int primer número
     * @param n2 int segundo número
     * @return resta de los números anteriores
     */
    public int resta(int n1, int n2) {
        return n1 - n2;
    }

    /**
     * Multiplica dos números enteros
     *
     * @param n1 int primer número
     * @param n2 int segundo número
     * @return multiplicación de entre los números anteriores
     */
    public int multiplica(int n1, int n2) {
        return n1 * n2;
    }

    public double divide(int n1, int n2) throws Exception  {
        if (n2 != 0) {
            return (double) n1 / n2;
        }
        throw new Exception("Error, división por 0");
    }

}
