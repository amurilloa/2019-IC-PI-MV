/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;
import util.Util;

/**
 *
 * @author allanmual
 */
public class TareaCalculadora {

    public static void main(String[] args) {
        // String t1 = Util.leerTexto("Digite un texto");
        // int num = Util.leerInt("#1");
        // double d1 = Util.leerDouble("#2");

        Calculadora calc = new Calculadora();

        String menu = "\nMenu Principal - UTN v0.1\n"
                + "1. Sumar\n"
                + "2. Restar\n"
                + "3. Multiplicar\n"
                + "4. Dividir\n"
                + "5. Historial\n"
                + "6. Salir";

        String msj = "";
        String historial = "";
        SALIR:
        do {
            int op = Util.leerInt(msj + menu);
            msj = "";
            switch (op) {
                case 1:
                    int n1 = Util.leerInt("1er número");
                    int n2 = Util.leerInt("2ndo número");
                    int t = calc.suma(n1, n2);
                    String res = String.format("Sumar: %d+%d=%d\n", n1, n2, t);
                    historial += res;
                    msj = res;
                    break;
                case 2:
                    n1 = Util.leerInt("1er número");
                    n2 = Util.leerInt("2ndo número");
                    t = calc.resta(n1, n2);
                    res = String.format("Restar: %d-%d=%d\n", n1, n2, t);
                    historial += res;
                    msj = res;
                    break;
                case 3:
                    n1 = Util.leerInt("1er número");
                    n2 = Util.leerInt("2ndo número");
                    t = calc.multiplica(n1, n2);
                    res = String.format("Multiplicar: %dx%d=%d\n", n1, n2, t);
                    historial += res;
                    msj = res;
                    break;
                case 4:
                    n1 = Util.leerInt("1er número");
                    n2 = Util.leerInt("2ndo número");
                    try {
                        double t1 = calc.divide(n1, n2);
                        res = String.format("Dividir: %d/%d=%.2f\n", n1, n2, t1);
                        historial += res;
                        msj = res;
                    } catch (Exception ex) {
                        res = String.format("Dividir: %d/%d=%s\n", n1, n2, "Error!");
                        msj = res;
                    }
                    break;
                case 5:
                    JOptionPane.showMessageDialog(null, historial);
                    break;
                case 6:
                    System.out.println("Gracias por utilizar la aplicación");
                    break SALIR;
                default:
                    msj = "Opción Inválida";
            }
        } while (true);
    }
}
