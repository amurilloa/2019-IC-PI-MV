/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class Ciclos {

    public static void main(String[] args) {

        int y = 1;
        int x1 = 1;

        while (y < 10) {
            x1 *= 2;
            System.out.println(x1);
        }

        int z = 1;
        while (z <= 2000) {
            int res = 5 * z;
            if (res > 1000) {
                break;
            }
            System.out.println("5x" + z + "=" + res);
            z++;
        }
        //Cuando la cantidad de iteraciones son finitas
        for (int i = 1; i <= 2000; i++) {
            int res = 5 * i;
            if (res > 1000) {
                break;
            }
            System.out.println("5x" + i + "=" + res);
        }

        int[] arreglo = {1, 2, 3, 4, 5, 6};

        for (int x : arreglo) {
            if (x % 2 == 0 && x > 2) {
                break;
            }
            System.out.println(x);
        }

        String[] palabras = {"Perro", "Gato", "Conejo", "Perico"};
        for (String palabra : palabras) {
            System.out.println(palabra);
        }

        String menu = "Menu Principal - UTN v0.1\n"
                + "1. Sumar\n"
                + "2. Restar\n"
                + "3. Multiplicar\n"
                + "4. Dividir\n"
                + "5. Salir";

        //Cuando no tengo la cantidad de iteraciones
        while (true) {
            int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
            System.out.println(op);
            if (op == 5) {
                break;
            }
        }
        int op = 0;
        do {
            op = Integer.parseInt(JOptionPane.showInputDialog(menu));
            System.out.println(op);
        } while (op != 5);

        int t = 0;
        int x = 20;
        do {
            t = x / 2;
            System.out.println("x: " + x + " t: " + t);
            x--;
        } while (t > 6);

    }
}
