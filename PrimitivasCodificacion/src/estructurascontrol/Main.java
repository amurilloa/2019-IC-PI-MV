/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {

        Calculadora calc = new Calculadora();

        String menu = "Menu Principal - UTN v0.1\n"
                + "1. Sumar\n"
                + "2. Restar\n"
                + "3. Multiplicar\n"
                + "4. Dividir\n"
                + "5. Salir";

        OUT:
        while (true) {
            try {
                int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
                switch (op) {
                    case 1:
                        int a = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                        int b = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                        int t = calc.suma(a, b);
                        JOptionPane.showMessageDialog(null, a + "+" + b + "=" + t);
                        break;
                    case 2:
                        a = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                        b = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                        t = calc.resta(a, b);
                        JOptionPane.showMessageDialog(null, a + "-" + b + "=" + t);
                        break;
                    case 3:
                        a = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                        b = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                        t = calc.multiplica(a, b);
                        JOptionPane.showMessageDialog(null, a + "*" + b + "=" + t);
                        break;
                    case 4:
                        a = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                        b = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                        try {
                            double tot = calc.divide(a, b);
                            JOptionPane.showMessageDialog(null, a + "/" + b + "=" + tot);
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                            JOptionPane.showMessageDialog(null, a + "/" + b + "= Error");
                        }
                        break;
                    case 5:
                        JOptionPane.showMessageDialog(null, "Muchas gracias por utilizar nuestra app");
                        break OUT;
                    default:
                        JOptionPane.showMessageDialog(null, "Opción Inválida");

                }
            } catch (NumberFormatException ex) {
                System.out.println(ex.getMessage());
                JOptionPane.showMessageDialog(null, "Favor digitar solo números");
            }
        }

//        int dia = Integer.parseInt(JOptionPane.showInputDialog("Digite un # de día(1-7): "));
//        switch (dia) {
//            case 1:
//                System.out.println("Domingo");
//                break;
//            case 2:
//                System.out.println("Lunes");
//                break;
//            case 3:
//                System.out.println("Martes");
//                break;
//            case 4:
//                System.out.println("Miércoles");
//                break;
//            case 5:
//                System.out.println("Jueves");
//                break;
//            case 6:
//                System.out.println("Viernes");
//                break;
//            case 7:
//                System.out.println("Sábado");
//                break;
//            default:
//                System.out.println("Número de día inválido");
//                
//        }
    }
}
