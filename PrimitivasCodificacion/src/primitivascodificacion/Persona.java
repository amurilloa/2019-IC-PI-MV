/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitivascodificacion;

/**
 *
 * @author allanmual
 */
public class Persona {

    public String nombre;
    public int edad;
    
    // public lo puede utilizar cualquier otra clase 
    // private lo puede utilizar unicamente la clase donde está declarado
    // nivel acceso + tipo retorno + nombre + parametros + retorno
    public int suma(int a, int b) {
        int total = a + b;
        return total;
    }
    
    public boolean esPar(){
        return edad%2==0; 
//        10|2
//       -10 5  --> /
//         0 --> %
       
    }

    public void hacenAlgo() {
        edad++;
    }
    

}

/*

cfhdfgdç
fhfgçfghf
ghj
*/