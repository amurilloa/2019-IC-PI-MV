/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitivascodificacion;

import java.math.MathContext;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class PrimitivasCodificacion {

    private String nombre; //Variable de Instancia o Atributo

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //pull -> Traer desde gitlab los cambios que no tengo en esta pc
        //commit --> salvar los cambios de manera local 
        //push --> Subir los cambios a la página de gitlab

        //Tipos de datos
        String text = "asd"; //Variables locales
        int num = 9;
        char letra = 'a';
        char digito = '\u2211';
        char otro = 97;
        System.out.println(digito + " = 12 \n");
        System.out.println("\u2211 = 12");
        System.out.println(otro);
        char sl = '\t';
        System.out.println(sl + "algo");
        boolean flag = false;
        byte by = 123;
        double d1 = 123.23;

        System.out.println("Hola");
        System.out.println(""); //sout + tab para generar esta línea de código
        System.out.println(letra);

        System.out.println("A\tB\tC\n1\t2\t3\n");
        System.out.println("\"HOLA\"");
        System.out.println('"' + "HOLA" + '"');
        System.out.println("El caracter de escape '\\n'  cambia de línea");
        System.out.println("El unicode \\u0040 vale \u0040");
        System.out.println("El unicode \\u00D1 vale \u00D1");
        char c = 64;

        System.out.println(c);

        //Variables
        //declar una variable 
        int numero;
        //inicializar la variable
        numero = 12;
        System.out.println(numero);
        //asignación de la variable 
        numero = 13;

        //Declaracion e inicialización de la variable
        int notaFinal = 0;
        int a1 = 123;
        int a2 = 123;

        final double VALOR_PI = 3.141592;

        boolean isReal = true;
        byte b = 122;
        short s = -29000;
        int i = 100000;
        long l = 999999999999L;
        float f1 = 234.99F;
        double d;
        char cvalue = '4';
        final double PI = 3.1415926;

        //Cating
        int cast = (int) 22.32;
        double d2 = (double) 5 / 2;
        System.out.println(cast);
        System.out.println(d2);
        byte b1 = (byte) 120;
        System.out.println(b1);

        //Operadores
        System.out.println("\nOperadores");
        System.out.println("12+12=" + (12 + 12));
        System.out.println("12-2=" + (12 - 2));
        System.out.println("12*2=" + (12 * 2));
        System.out.println("5/2=" + (5 / 2)); //Si los dos operadores son enteros el resultado es entero. 
        System.out.println("5%2=" + (5 % 2)); //Si los dos operadores son enteros el resultado es entero. 

        double total = 7.5 * 2;
        System.out.println(total);

//        5 |2
//      - 4  2  -> Es resultado de la división /
//        1  -> Residuo y es el resultado de aplicar %  
        //Operadores unarios ++ | --
        int cont = 1;
        int d3 = cont--;
        System.out.println("cont: " + cont);
        System.out.println("d3: " + d3);

        // Operadores de asignación 
        cont = 10;
        cont += 5;
        System.out.println("cont: " + cont);
        //cont = cont + 5; // cont +=5;

        //Entrada y salida de datos
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Digite su nombre: ");
//        String nom = sc.nextLine();
//        System.out.println(nom);
//        
//        System.out.print("Digite su edad: ");
//        int a = Integer.parseInt(sc.nextLine());
//        System.out.println("Edad: " + a);
        //Mostrar un mensaje
        //String nombre = JOptionPane.showInputDialog("Digite su nombre: ");
        //int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite su edad: "));
        //JOptionPane.showMessageDialog(null, "Nombre: " + nombre + "\nEdad: " + edad, "Ejemplo", JOptionPane.PLAIN_MESSAGE);
//          JOptionPane.showMessageDialog(null, "Mensaje", "Información",JOptionPane.INFORMATION_MESSAGE);
//          JOptionPane.showMessageDialog(null, "Mensaje", "Error",JOptionPane.ERROR_MESSAGE);
//          JOptionPane.showMessageDialog(null, "Mensaje", "Warning",JOptionPane.WARNING_MESSAGE);
//          JOptionPane.showMessageDialog(null, "Mensaje", "Sin Icono",JOptionPane.PLAIN_MESSAGE);
//          JOptionPane.showMessageDialog(null, "Algo de Texto");
        Persona p = new Persona();//Instancia de la clase 
        p.nombre = "Allan";
        p.edad = 30;
        
        
        Persona p1 = new Persona();//Instancia de la clase 
        p1.nombre = "Lucía";
        p1.edad = 3;
        
        int patito1 = 10;
        int patito2 = 9;
        
        int x = p.suma(patito1, patito2);
        System.out.println(x);
        System.out.println(p.esPar());
        System.out.println(p1.esPar());
        System.out.println(p.nombre);
        System.out.println(p.edad);
        System.out.println(p1.nombre);
        System.out.println(p1.edad);
        
        
        System.out.println(Math.abs(12));
        
        
        
        
//        int x = p.suma(8,6); //paso de parametro por valor
//        System.out.println(x);
//        x = p.suma(12,3);//12,3
//        System.out.println(x);
//        x = p.suma(11,5);//11,4
//        System.out.println(x);
//        p.hacenAlgo();


        //Clase      Instancia/Objeto
        //Persona    p, p1




    }

}
