/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author allanmual
 */
public class Logica {

    private int[] arreglo;

    public Logica() {
        arreglo = new int[0];
    }

    /**
     * Sumar los elementos de un arreglo
     *
     * @return int sumatoria de los valores de arreglo
     */
    public int sumar() {
        int suma = 0;

        for (int x : arreglo) {
            suma += x;
        }
        return suma;
    }

    public double promedio() {
        int suma = sumar();
        return (double) suma / arreglo.length;
    }

    public int mayor() throws Exception {
        if (arreglo.length > 0) {
            int mayor = arreglo[0];

            for (int x : arreglo) {
                if (x > mayor) {
                    mayor = x;
                }
            }
            return mayor;
        }
        throw new Exception("No hay datos registrados!!");
    }

    public int menor() throws Exception {
        if (arreglo.length > 0) {
            int menor = arreglo[0];

            for (int x : arreglo) {
                if (x < menor) {
                    menor = x;
                }
            }
            return menor;
        }
        throw new Exception("No hay datos registrados!!");
    }

    public void setArreglo(int[] arreglo) {
        if (arreglo != null) {
            this.arreglo = arreglo;
        }
    }

    public void generar() {
        int tam = (int) (Math.random() * 10) + 1;
        int max = 25;
        int min = 1;
        arreglo = new int[tam];
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = (int) (Math.random() * max) + min;
        }
    }

    public String imprimir() {
        String txt = "";

        for (int x : arreglo) {
            txt += x + ",";
        }
        txt = txt.substring(0, txt.length() - 1);

        return txt;
    }

}
