/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import util.Util;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();

        int[] numeros = {12, 42, 11, 4, 5, 54, 6, 20, 9, 10};
        log.setArreglo(numeros);

        String menu = "Menu Principal - UTN v0.1\n"
                + "1. Sumar valores de un arreglo\n"
                + "2. Calcular un promedio de los valores del arreglo\n"
                + "3. Valor máximo en un arreglo\n"
                + "4. Valor mínimo en un arreglo\n"
                + "5. Generar nuevo arreglo\n"
                + "6. Imprimir arreglo\n"
                + "7. Salir";
        String res = "";
        OUT:
        do {
            int op = Util.leerInt(menu + res);
            res = "";
            switch (op) {
                case 1:
                    int total = log.sumar();
                    res = "\n\n>>>>> Total: " + total;
                    break;
                case 2:
                    double prom = log.promedio();
                    res = String.format("\n\n>>>>> Promedio: %.2f", prom);
                    break;
                case 3:
                    try {
                        int mayor = log.mayor();
                        res = "\n\n>>>>> Mayor: " + mayor;
                    } catch (Exception e) {
                        res = "\n\n**** " + e.getMessage() + "****";
                    }
                    break;
                case 4:
                    try {
                        int menor = log.menor();
                        res = "\n\n>>>>> Menor: " + menor;
                    } catch (Exception e) {
                        res = "\n\n**** " + e.getMessage() + "****";
                    }
                    break;
                case 5:
                    log.generar();
                case 6:
                    res = "\n\n>>>> Arreglo: " + log.imprimir();
                    break;
                case 7:
                    break OUT;
                default:
                    res = "\n\n **** Opción Inválida!!  ****";
            }
        } while (true);

    }
}
