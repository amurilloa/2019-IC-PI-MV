/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

import com.sun.org.apache.bcel.internal.generic.GOTO;
import util.Util;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();

        String menu = "Gato UTN - v0.1\n"
                + "1. Nuevo Juego\n"
                + "2. Continuar Juego\n"
                + "3. Estadísticas\n"
                + "4. Salir\n";

        String jugando = "Jugando...%s\n"
                + "%s\n"
                + "Donde desea marcar(número,letra)";

        APP:
        while (true) {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    String nom1 = Util.leerTexto("Nombre del Primer Jugador");
                    String nom2 = Util.leerTexto("Nombre del Segundo Jugador");
                    //Registrar los Jugadores
                    Jugador j1 = new Jugador(nom1);
                    Jugador j2 = new Jugador(nom2);
                    log.registrar(j1, j2);
                case 2:
                    log.iniciar();
                    //Jugar
                    String res = "";
                    JUEGO:
                    while (true) {
                        boolean termino = false;
                        String msj = res + String.format(jugando, log.getNomAct(), log.imprimir());
                        String txt = Util.leerTexto(msj);
                        if ("salir".equals(txt)) {
                            break;
                        }
                        res = "";
                        try {
                            if (!log.jugar(txt)) {
                                res = "Espacio ya estaba marcado, favor intente nuevamente\n\n";
                            }
                            if (log.gano()) {
                                Util.mostrar("Felicidades..." + log.getGanador() + " has ganado!!");
                                termino = true;
                            } else if (log.empate()) {
                                Util.mostrar("Juego empatado");
                                termino = true;
                            }

                            if (termino) {
                                if (Util.confirmar("¿Desean continuar?")) {
                                    log.iniciar();
                                } else {
                                    break JUEGO;
                                }
                            }

                        } catch (Exception ex) {
                            res += ex.getMessage() + "\n\n";
                        }
                    }
                    break;

                case 3:
                    break;
                case 4:
                    break APP;
                default:
            }
        }
    }

}
