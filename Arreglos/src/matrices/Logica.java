/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author allanmual
 */
public class Logica {

    private Jugador j1;
    private Jugador j2;
    //Temporal para saber quién está jugando
    private Jugador actual;
    private char[][] gato;
    private boolean turno;

    public Logica() {
        iniciar();
    }

    public void iniciar() {
        gato = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    }

    public String imprimir() {
        String res = "  A B C\n";
        for (int fila = 0; fila < gato.length; fila++) {
            res += (fila + 1) + " ";
            for (int columna = 0; columna < gato[fila].length; columna++) {
                res += gato[fila][columna];
                res += columna == gato[fila].length - 1 ? "" : "|";
            }
            res += "\n";
        }
        return res;
    }

    public boolean jugar(String texto) {
        texto = texto.replaceAll(" ", "").toLowerCase();
        if (!texto.contains(",") || texto.length() != 3) {
            throw new RuntimeException("Formato(número,letra) de entrada inválido");
        }
        String[] datos = texto.split(",");
        int c = "a".equals(datos[1]) ? 1 : "b".equals(datos[1]) ? 2 : 3;
        int f;
        try {
            f = Integer.parseInt(datos[0]);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Formato(número,letra) de entrada inválido");
        }
        return jugar(f, c);
    }

    public boolean jugar(int f, int c) {
        f--;
        c--;
        if (f >= gato.length || f < 0 || c >= gato[f].length || c < 0) {
            throw new RuntimeException("Letra o número inválidos!!");
        }
        if (gato[f][c] == '-') {
            gato[f][c] = !turno ? 'X' : 'O';
            turno = !turno;
            actual = actual.equals(j1) ? j2 : j1;
            return true;
        }
        return false;
    }

    public void registrar(Jugador j1, Jugador j2) {
        int num = (int) (Math.random() * 10) + 1;
        this.j1 = j1;
        this.j2 = j2;
        actual = num > 5 ? j2 : j1;

    }

    public String getNomAct() {
        if (actual == null) {
            return "Desconocido";
        }
        return actual.getNombre();
    }

    public boolean gano() {
        return true;
    }

    boolean empate() {
        return true;
    }

    public String getGanador() {
        return actual.equals(j1) ? j2.getNombre() : j1.getNombre();
    }
}
