/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author allanmual
 */
public class Arreglos {

    public static int[] convertirPar(int[] arreglo) {
        //Aqui le hacemos algo al arreglo
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                arreglo[i] = arreglo[i] + 1;
            }
        }
        return arreglo;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int[] a = new int[5];
        a[0] = 12;
        a[2] = 4;
        a[4] = a[0] + a[2];

        //Consultar como modificar los datos del arrego
        for (int i = 0; i < a.length; i++) {
            //System.out.println(a[i]);
            a[i] = (int) (Math.random() * 10) + 1;
        }

        //Perfecto para consultar los datos
        for (int x : a) {
            System.out.print(x + ",");
        }
        System.out.println("");
        String[] palabras = {"Hola", "Adios", "Perro", "Ratón", "Gato"};

        String[] palabras2 = new String[5];
        palabras2[0] = "Hola";
        palabras2[1] = "Adios";
        palabras2[2] = "Perro";
        palabras2[3] = "Ratón";
        palabras2[4] = "Gato";

        String[] dias = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves",
            "Viernes", "Sábado"};
        
        int d = (int) (Math.random() * dias.length);
        System.out.println(dias[d]);

        int[][] matriz;

        int var = 15;
        a[1] = var;
        // System.out.println(a[1]);
        var = a[2];
        // System.out.println(var);

        int[] numeros = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        numeros = convertirPar(numeros);
        for (int numero : numeros) {
            System.out.println(numero);
        }


    }

}
