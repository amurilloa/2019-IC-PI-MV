/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author allanmual
 */
public class Main {

    public static void main(String[] args) {

        String txt = "Hola Mundo";
        //|H|o|l|a| |M|u|n|d|o| ---> 10 Largo
        //|0|1|2|3|4|5|6|7|8|9| ---> 9 Último elemento

        System.out.println("Largo: " + txt.length());

        int ue = (txt.length() - 1);
        System.out.println("Indice Último elemnto: " + ue);

        char letra = txt.charAt(ue); //Obtiene una letra/char de indice específico
        System.out.println("Último elemento: " + letra);

//        for (int i = 0; i < txt.length(); i++) {
//            System.out.println(txt.charAt(i));
//        }
        char[] arreglo = txt.toCharArray();//Convierte en un arreglo de chars
//        for (char let : arreglo) {
//            System.out.println(let);
//        }

        //Arreglo de chars a String 
        String x = new String(arreglo);

        String z = null;
        System.out.println(x.equals(z)); //Para comparar Strings u Objetos
        System.out.println("hola".equals(z));
        // System.out.println(z.equals("hola"));
        System.out.println("hola".equals("Hola"));
        System.out.println("hola".equalsIgnoreCase("Hola"));//Compara sin tomar en cuenta mayusculas y minusculas
        if ("hola".equals("Hola")) {
            System.out.println("son iguales");
        } else {
            System.out.println("no son iguales");
        }

        boolean flag = "hola".equals("Hola");
        System.out.println(" hola ".trim()); //Elimina los espacios del inicio y el final
        String a = " hola ";
        System.out.println("hola".equals(a.trim()));
        System.out.println(a.trim().equals(x));
        a = a.trim();
        System.out.println(a);
        a = a.toUpperCase();
        System.out.println(a);
        a = a.toLowerCase();
        System.out.println(a);

        System.out.println(txt.substring(5));
        System.out.println(txt.substring(3, 6));//[3,6[

        String c1 = "Amarillo";
        String colores = "Amarillo,Rojo,Azul,Verde";

        String[] col = colores.split(",");//Separa los elemtos por un caracter

        for (String c : col) {
            System.out.println(c);
        }

        System.out.println(colores.contains(c1)); //Busca si una palabra esta contenida en otra

        System.out.println(colores.replaceAll("[^aeiou]", "x"));
        System.out.println("hola hola".indexOf("l")); //Busca el indice de una letra en el texto
        System.out.println("hola hola".indexOf("l",3));//Busca el indice de una letra en el texto, pero a partir de un indice específico
        System.out.println("hola hola".indexOf("l",8));
        System.out.println("hola hola".indexOf("x"));
        System.out.println("hola hola hola".lastIndexOf("l"));//Busca el último índice donde aparece la letra
        

    }
}
