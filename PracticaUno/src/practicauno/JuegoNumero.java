/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicauno;

import util.ManejoArchivos;

/**
 *
 * @author allanmual
 */
public class JuegoNumero {

    private int numero;
    private int intentos;
    private int nivel;//1-3 1:10, 2:25, 3:50
    private final int MAX_INT = 5;
    private final String RUTA_REC = "datos/records.txt";

    public JuegoNumero() {

    }

    public void iniciarJuego(int nivel) {
        this.nivel = nivel;
        int max = nivel == 3 ? 50 : nivel == 2 ? 25 : 10; //String txt = true ? "verdad":"false"
        numero = (int) (Math.random() * max) + 1;
        intentos = 1;
    }

    public boolean jugar(int num) {
        if (num == numero) {
            return true;
        } else {
            intentos++;
            return false;
        }
    }

    public boolean perdi() {
        return intentos > MAX_INT;
    }

    public String pista(int num) {
        return num < numero ? "Digite un número mayor" : "Digite un número menor";
    }

    public void record(String nombre) {
//        String dif = nivel == 3 ? "Dificil" : nivel == 2 ? "Medio" : "Fácil";
//        String txt = String.format("%s ganó en %d intentos en dificultad %s",
//                nombre, intentos, dif);
        String txt = String.format("%s,%d,%d", nombre, intentos, nivel);
        ManejoArchivos ma = new ManejoArchivos();
        ma.escribir(RUTA_REC, txt);
    }

    public String getRecords() {
        ManejoArchivos ma = new ManejoArchivos();
//        String[] datos = ma.leer(RUTA_REC).split("\n");
//        for (String dato : datos) {
//            System.out.println(dato);
//            String[] temp = dato.split(",");
//            System.out.println(temp[0]);
//        }
        return ma.leer(RUTA_REC);
    }

    public int getNumero() {
        if (intentos > MAX_INT) {
            return numero;
        } else {
            return -1;
        }
    }

    public int getIntentos() {
        return intentos;
    }

    public int getNivel() {
        return nivel;
    }

    @Override
    public String toString() {
        return "JuegoNumero{" + "numero=?" + ", intentos=" + intentos + ", nivel=" + nivel + ", MAX_INT=" + MAX_INT + '}';
    }

}
