/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicauno;

import util.Util;

/**
 *
 * @author allanmual
 */
public class PracticaUno {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JuegoNumero juego = new JuegoNumero();
        
        String menu = "\n Adivinar Número - UTN v0.1\n"
                + "1. Iniciar Juego\n"
                + "2. Records\n"
                + "3. Salir";

        String menu2 = "\nNiveles de Dificultad\n"
                + "1. Fácil   (1-10)\n"
                + "2. Medio   (1-25)\n"
                + "3. Difícil (1-50)\n"
                + "4. Volver";

        OUT:
        while (true) {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    //Solicitamos el nivel
                    int nivel = Util.leerInt(menu2);
                    if (nivel == 4) {
                        break;
                    }
                    //Empezamos el juego
                    juego.iniciarJuego(nivel);
                    String msj = "Digite un número";
                    do {
                        int num = Util.leerInt(msj);
                        if (juego.jugar(num)) {
                            String nombre = Util.leerTexto("Digite su nombre: ");
                            juego.record(nombre);
                            String text = String.format("Felicidades....ganaste en %d intentos", juego.getIntentos());
                            Util.mostrar(text);
                            break;
                        } else if (juego.perdi()) {
                            Util.mostrar("No quedan más intentos, el número era: " + juego.getNumero());
                            break;
                        } else {
                            msj = juego.pista(num) + ", último número: " + num;
                        }
                    } while (true);
                    break;
                case 2:
                    String records = juego.getRecords();
                    Util.mostrar(records);
                    break;
                case 3:
                    Util.mostrar("Gracias por utilizar nuestra aplicación.");
                    break OUT;
                default:
                    System.out.println("Inválido");
                    break;
            }
        }
    }

}
