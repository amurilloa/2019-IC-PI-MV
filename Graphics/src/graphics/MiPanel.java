/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel {

    public MiPanel() {
        setBackground(Color.white);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(400, 500);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

//        g.setColor(new Color(255,255,255));
//        g.drawLine(0, 150, 400, 150);
//        g.drawLine(200, 0, 200, 300);
//        g.setColor(Color.blue);
//        g.drawOval(150, 100, 100, 100);
//        g.fillOval(155, 105, 90, 90);
//        g.setFont(new Font("Patito", Font.BOLD, 24));
//        g.drawString("Hola Mundo", 140, 35);
//        g.fillArc(100, 100, 50, 50, 180, 90);
//        g.fillPolygon(xs, ys, xs.length);
//        g.setColor(Color.WHITE);
//        g.drawLine(220, 60, 260, 60);
//        g.drawLine(220, 61, 260, 61);
//        g.drawLine(220, 62, 260, 62);
        int x = 200;
        int y = 250;

        g.fillArc(50, 60, 300, 400, 50, 75);
        g.fillOval(95, 230, 210, 200);
        g.setColor(new Color(244, 202, 82));
        g.fillRect(95, 230, 210, 100);
        g.setColor(Color.black);
        int[] xs = {200, 305, 305, 200, 95, 95};
        int[] ys = {250, 230, 30, 200, 30, 230};
        g.fillPolygon(xs, ys, xs.length);
        g.setColor(new Color(189, 190, 192));
        g.fillOval(130, 90, 140, 140);
        g.setColor(new Color(254, 254, 254));
        g.fillOval(140, 100, 120, 120);
        g.setColor(new Color(99, 55, 18));
        g.fillOval(170, 130, 60, 60);
        g.setColor(Color.black);
        g.fillOval(185, 145, 30, 30);
        int[] x2 = {200, 305, 305, 95, 95};
        int[] y2 = {300, 260, 340, 340, 260};
        g.fillPolygon(x2, y2, x2.length);
        g.setColor(new Color(251, 242, 0));
        g.fillOval(160, 315, 80, 30);

        //g.fillOval(WIDTH, WIDTH, WIDTH, HEIGHT);
        g.setColor(Color.red);
        g.drawLine(0, y, 400, y); //x
        g.drawLine(x, 0, x, 500); //7
    }
}
