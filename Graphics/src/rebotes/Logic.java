/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rebotes;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author allanmual
 */
public class Logic {

    private Circle cir1;
    private Circle cir2;
    private Circle cir3;
    private Circle cir4;
    private Circle[] circulos;
    private int can;
    private int height;
    private int width;

    public Logic(int width, int height) {
        this.height = height;
        this.width = width;
        cir1 = new Circle(Color.BLUE, 280, 280, 40, 2, true);
        cir2 = new Circle(Color.RED, 280, 280, 40, 4, true);
        cir3 = new Circle(Color.GREEN, 280, 280, 40, 6, true);
        cir4 = new Circle(Color.MAGENTA, 280, 280, 40, 8, true);
        cir1.setDir(1);
        cir2.setDir(2);
        cir3.setDir(3);
        cir4.setDir(4);
        circulos = new Circle[]{cir1, cir2, cir3, cir4};

    }

    public void run(Graphics g) {
        for (Circle cir : circulos) {
            cir.paint(g);
            cir.move();
            cir.bounce(height, width);
        }
    }
}
