/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rebotes;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author allanmual
 */
public class Circle {

    private Color color;
    private int x;
    private int y;
    private int size;
    private int speed;
    private boolean fill;
    private int dir;

    public Circle() {
        color = Color.BLACK;
        size = 40;
        fill = true;
        speed = 1;
    }

    public Circle(int x, int y) {
        this.x = x;
        this.y = y;
        color = Color.BLACK;
        size = 40;
        fill = true;
        speed = 1;
    }

    public Circle(Color color, int x, int y, int size, int speed, boolean fill) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.size = size;
        this.speed = speed;
        this.fill = fill;
    }

    public void paint(Graphics g) {

        g.setColor(color);

        if (fill) {
            g.fillOval(x, y, size, size);
        } else {
            g.drawOval(x, y, size, size);
        }
    }

    public void move() {
        //0 --> detenido
        //1 --> izq
        //2 --> arriba
        //3 --> der
        //4 --> abajo
        //5 --> dia sup izq
        //6 --> dia sup der
        //7 --> dia inf der
        //8 --> dia inf izq
        switch (dir) {
            case 0:
                break;
            case 1:
                x -= speed;
                break;
            case 2:
                y -= speed;
                break;
            case 3:
                x += speed;
                break;
            case 4:
                y += speed;
                break;
            case 5:
                x -= speed;
                y -= speed;
                break;
            case 6:
                x += speed;
                y -= speed;
                break;
            case 7:
                x += speed;
                y += speed;
                break;
            case 8:
                x -= speed;
                y += speed;
                break;
        }

    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    /**
     *
     * @param height
     * @param width
     */
    public void bounce(int height, int width) {
        if (x < 0) {
            x = 0;
            dir = newDir(dir, true);
        }

        if (y < 0) {
            y = 0;
            dir = newDir(dir, false);
        }

        if (x + size > width) {
            x = width - size;
            dir = newDir(dir, true);
        }

        if (y + size > height) {
            y = height - size;
            dir = newDir(dir, false);
        }

    }

    private int newDir(int act, boolean bounceX) {
        int rifa = (int) (Math.random() * 3);
        
        if (act == 1) {
            int[] pos = {3, 6, 7};
            return pos[rifa];
        } else if (act == 2) {
            int[] pos = {4, 7, 8};
            return pos[rifa];
        } else if (act == 3) {
            int[] pos = {1, 5, 8};
            return pos[rifa];
        } else if (act == 4) {
            int[] pos = {2, 5, 6};
            return pos[rifa];
        } else if (act == 5) {
            int a = bounceX ? 6 : 8;
            int b = bounceX ? 3 : 4;
            int[] pos = {a, b, 7};
            return pos[rifa];
        } else if (act == 6) {
            int a = bounceX ? 5 : 7;
            int b = bounceX ? 1 : 4;
            int[] pos = {a, b, 8};
            return pos[rifa];
        } else if (act == 7) {
            int a = bounceX ? 8 : 6;
            int b = bounceX ? 1 : 2;
            int[] pos = {a, b, 5};
            return pos[rifa];
        } else if (act == 8) {
            int a = bounceX ? 7 : 5;
            int b = bounceX ? 3 : 2;
            int[] pos = {a, b, 6};
            return pos[rifa];
        }

        return 0;
    }

}
