/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel implements KeyListener {

    private Logic log;

    public MiPanel() {
        setBackground(Color.WHITE);
        log = new Logic(600, 500);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 500);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        int y = getHeight() / 2;//300
        int x = getWidth() / 2;//300

        log.run(g);

        g.setColor(Color.RED);
        g.drawLine(0, y, getWidth(), y);
        g.drawLine(x, 0, x, getHeight());

        //Cuadricula
        g.setColor(new Color(230, 230, 231));
        x = 0;
        y = 0;
        for (int i = 0; i <= 15; i++) {
            g.drawLine(0, y, getWidth(), y);
            g.drawLine(x, 0, x, getHeight());
            x += 40;
            y += 40;
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
//        System.out.println("Typed");//cuando ya veo lo que escribo
//        System.out.println(e.getKeyChar());
//        System.out.println(e.getKeyCode());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        log.acciones(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //log.acciones(e.getKeyCode(), false);   
    }

}
