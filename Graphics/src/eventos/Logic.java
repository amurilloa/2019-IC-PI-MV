/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventos;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Logic {

    private int can;
    private boolean pause;
    private boolean inicio;
    private boolean saliendo;

    private final int x;
    private final int y;
    private final int width;
    private final int height;
    private final LinkedList<Circle> circles;

    private final Color COL_DEF = Color.BLUE;

    public Logic(int width, int height) {
        this.height = height;
        this.width = width;
        int size = 40;
        x = width / 2;
        y = height / 2;
        Circle cir = new Circle(COL_DEF, x - size / 2, y - size / 2, size, 4, true);
        circles = new LinkedList<>();
        circles.add(cir);
    }

    public void run(Graphics g) {
        if (saliendo) {
            g.setFont(new Font("Patito3", Font.BOLD, 40));
            g.setColor(Color.BLACK);
            g.drawString("Y: YES | N: NO", x - 143, y + 15);
        } else if (inicio && !pause) {
            for (Circle cir : circles) {
                cir.paint(g);
                cir.move();
                cir.bounce(height, width);
                cir.bounce(circles);
            }
            if (circles.isEmpty()) {
                int size = 40;
                circles.add(new Circle(COL_DEF, x - size / 2, y - size / 2, size, 4, true));
                int ale = (int) (Math.random() * 4) + 1;
                circles.getLast().setDir(ale);
            }
        } else if (pause) {
            g.setFont(new Font("Patito2", Font.BOLD, 40));
            g.setColor(Color.BLACK);
            g.drawString("PAUSE", x - 68, y + 15);
        } else {
            g.setFont(new Font("Patito1", Font.BOLD, 40));
            g.setColor(Color.BLACK);
            g.drawString("PRESS \u21D4\u21D5 FOR START", x - 225, y + 15);
        }

        g.setFont(new Font("Patito", Font.BOLD, 13));
        g.setColor(Color.GRAY);
        g.drawString("X: EXIT", x + 233, y - 220);

    }

    public void acciones(int keyCode) {
        Circle cir = circles.getLast();
        if (keyCode >= KeyEvent.VK_LEFT && keyCode <= KeyEvent.VK_DOWN && !inicio) {
            inicio = true;
            cir.setDir(keyCode - 36);
            //cir.setDir(0);
            cir.setX(x - cir.getSize() / 2);
            cir.setY(y - cir.getSize() / 2);
            cir.setColor(COL_DEF);
        } else if (keyCode == KeyEvent.VK_S) {
            cir.setDir(0);
            circles.clear();
            circles.addFirst(cir);
            inicio = false;
            pause = false;
        } else if (keyCode == KeyEvent.VK_P && inicio) {
            pause = !pause;
        } else if (keyCode == KeyEvent.VK_X) {
            saliendo = true;
        } else if (saliendo && keyCode == KeyEvent.VK_Y) {
            System.exit(0);
        } else if (saliendo && keyCode == KeyEvent.VK_N) {
            saliendo = false;
        } else if (inicio && !pause) {
            if (keyCode >= 49 && keyCode <= 53) {
                cir.setColor(buscarColor(keyCode - 48));
            } else if (keyCode == KeyEvent.VK_A) {
                int size = cir.getSize();
                int ale = (int) (Math.random() * 5) + 1;
                Color col = buscarColor(ale);
                Circle temp = new Circle(col, x - size / 2, y - size / 2, size, 4, true);
                ale = (int) (Math.random() * 4) + 1;
                temp.setDir(ale);
                circles.add(temp);
            }
        }
        //TODO: Extra: Si presionan la A, agregar una nueva bola. #LinkedList     
    }

    /**
     * Selecciona un color a partir de una opción
     *
     * @param op int entre [1-5]
     * @return Color dependiendo del número elegido
     */
    private Color buscarColor(int op) {
        switch (op) {
            case 1:
                return COL_DEF;
            case 2:
                return Color.CYAN;
            case 3:
                return Color.GREEN;
            case 4:
                return Color.ORANGE;
            case 5:
                return Color.PINK;
            default:
                return Color.BLACK;
        }
    }

}
