/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosmovimiento;

import java.awt.Color;
import java.awt.Graphics;
import util.Util;

/**
 *
 * @author allanmual
 */
public class Carro {

    //Atributos Lógicos
    private String placa;
    private String marca;
    private String modelo;

    //Atributos gráficos
    private boolean encendido;
    private Color color;
    private int x;
    private int y;
    private int can;

    public Carro() {
        color = Color.BLUE;
    }

    public Carro(String placa, String marca, String modelo) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        color = Color.BLUE;
    }

    public Carro(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g) {
        //Carrocería
        int[] xs = {50, 80, 100, 165, 205, 250, 250, 230, 50};
        int[] ys = {30, 30, 0, 0, 30, 35, 60, 65, 65};
        Util.inc(x, xs);
        Util.inc(y, ys);
        g.setColor(color);
        g.fillPolygon(xs, ys, xs.length);

        //Llantas
        g.setColor(Color.BLACK);
        g.fillOval(x + 77, y + 47, 36, 36);
        g.fillOval(x + 202, y + 47, 36, 36);

        //Ventana trasera
        int[] xsVT = {85, 102, 125, 125};
        int[] ysVT = {30, 5, 5, 30};
        Util.inc(x, xsVT);
        Util.inc(y, ysVT);
        g.setColor(Color.WHITE);
        g.fillPolygon(xsVT, ysVT, xsVT.length);

        //Ventana delantera
        int[] xsVD = {130, 163, 198, 130};
        int[] ysVD = {5, 5, 30, 30};
        Util.inc(x, xsVD);
        Util.inc(y, ysVD);
        g.setColor(Color.WHITE);
        g.fillPolygon(xsVD, ysVD, xsVD.length);

        //Bumpers
        g.setColor(Color.BLACK);
        g.fillRect(x + 250, y + 50, 5, 10);
        g.fillRect(x + 45, y + 55, 5, 10);

        //Luz
        if (encendido) {
            g.setColor(Color.YELLOW);
        } else {
            g.setColor(Color.WHITE);
        }

        g.fillRect(x + 244, y + 35, 5, 8);

        //Luz Freno
        g.setColor(Color.RED);
        g.fillRect(x + 51, y + 31, 5, 10);

        if (encendido) {
            System.out.println(can);
            //Humo
            g.setColor(Color.GRAY);
            switch (can) {
                case 3:
                    g.fillOval(x + 27, y + 50, 12, 12);
                case 2:
                    g.fillOval(x + 23, y + 56, 12, 12);
                case 1:
                    g.fillOval(x + 33, y + 59, 12, 12);
                    break;
                default:
                    can = 0;
            }
            can++;
        }
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Carro{" + "placa=" + placa + ", marca=" + marca + ", modelo=" + modelo + ", encendido=" + encendido + ", color=" + color + ", x=" + x + ", y=" + y + '}';
    }

}
