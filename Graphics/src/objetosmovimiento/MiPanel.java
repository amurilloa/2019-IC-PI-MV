/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosmovimiento;

import graphics.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MiPanel extends JPanel {

    private Carro c1;
    private Carro c2;

    public MiPanel() {
        setBackground(Color.white);
        c1 = new Carro();
        c2 = new Carro(Color.GREEN, 0, 200);
        c2.setEncendido(true);

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 600);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        int y = getHeight() / 2;//300
        int x = getWidth() / 2;//300

        c1.paint(g);
        c2.paint(g);
        c2.setX(c2.getX() + 5);

        int dim = 100;
        int tam = 50;
        g.fillOval(x, y, dim, dim);

        g.setColor(Color.BLACK);
        g.fillOval(x, y, dim * tam / 100, dim * tam / 100);

        g.setColor(Color.RED);
        g.drawLine(0, y, getWidth(), y);
        g.drawLine(x, 0, x, getHeight());
    }
}
