/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenunoallanmurillo;

/**
 *
 * @author allanmual
 */
public class Logica {

    /**
     * Método que recibe por parámetro dos números enteros, y retorna un String
     * con los números desde el primero hasta el segundo, puede que el primero
     * sea mayor al segundo o viceversa.
     *
     * @param a int Primer número
     * @param b int Segundo número
     * @return String con los elementos desde a hasta b
     */
    public String ejercicioA(int a, int b) {
        String res = "";
        if (a > b) {
            for (int i = a; i >= b; i--) {
                res += i + ",";
            }
        } else {
            for (int i = a; i <= b; i++) {
                res += i + ",";
            }
        }

        return res + "\b";
    }

    /**
     * Método que genera un arreglo de números enteros sin repetir desde 1 a N
     *
     * @param n int límite superior de los números a generar
     * @param dim int cantidad de elementos a generar
     * @return int[] arreglo con los números generados
     */
    public int[] ejercicioB(int n, int dim) {
        int[] res = new int[dim];
        if (dim - n > 0) {
            String r = "Imposible generar %d números del 1 al %d sin repetir";
            throw new RuntimeException(String.format(r, dim, n));
        }

        int pos = 0;
        while (pos < dim) {
            int a = (int) (Math.random() * n) + 1;
            if (!esta(a, res)) {
                res[pos++] = a;
            }
        }
        return res;
    }

    /**
     * Revisa si el número n existe o no en el arreglo
     *
     * @param numero int número a buscar
     * @param arreglo int[] elementos dónde se buscará el número
     * @return
     */
    private boolean esta(int numero, int[] arreglo) {
        for (int elemento : arreglo) {
            if (elemento == numero) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determina si un número es primo o no
     *
     * @param num int número a evaluar
     * @return true si el número es primo o false caso contrario
     */
    public boolean ejercicioC(int num) {
        int div = 0;
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                div++;
            }
        }
        return div == 2;
    }
}
