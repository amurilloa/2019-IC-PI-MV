/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenunoallanmurillo;

import java.util.Arrays;
import util.Util;

/**
 *
 * @author allanmual
 */
public class ExamenUnoAllanMurillo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();

        String menu = "Menu Principal\n"
                + "i.   Imprimir números\n"
                + "ii.  Generar arreglo\n"
                + "iii. Número primo\n"
                + "iv.  Salir";
        APP:
        while (true) {
            String op = Util.leerTexto(menu).toLowerCase();
            switch (op) {
                case "i":
                    int a = Util.leerInt("Desde");
                    int b = Util.leerInt("Hasta");
                    Util.mostrar(log.ejercicioA(a, b));
                    break;
                case "ii":
                    try {
                        int n = Util.leerInt("Límite superior(1-N)");
                        int dim = Util.leerInt("Cantidad de números a generar");
                        Util.mostrar(Arrays.toString(log.ejercicioB(n, dim)));
                    } catch (Exception ex) {
                        Util.mostrar(ex.getMessage());
                    }
                    break;
                case "iii":
                    int num = Util.leerInt("Número a evaluar; ");
                    String res = log.ejercicioC(num) ? "" : "no ";
                    String txt = String.format("El número %d %ses primo", num, res);
                    Util.mostrar(txt);
                    break;
                case "iv":
                    if (Util.confirmar("¿Está seguro que desea salir?")) {
                        Util.mostrar("Gracias por utilzar nuestra aplicación.");
                        break APP;
                    }
                    break;
            }
        }

        Persona p = new Persona();
        p.setCedula("206470762");
        p.setNombre("Allan");
        p.setApellidoUno("Murillo");
        p.setApellidoDos("Alfaro");
        p.setCorreo("allanmual@gmail.com");
        p.setTelefono("8526-2638");
        Util.mostrar(p.obtenerNombre());
        Util.mostrar(p.toString());
    }
}
