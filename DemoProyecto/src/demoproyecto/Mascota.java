/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyecto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author allanmual
 */
public class Mascota {

    private String nombre;
    private String raza;
    private String color;
    private Date fechaNacimiento;
    private boolean pedidree;
    private double peso;

    private String cedula;//Cédula del dueño de la mascota

    public Mascota() {
    }

    public Mascota(String nombre, String raza, String color, Date fechaNacimiento, boolean pedidree, double peso, String cedula) {
        this.nombre = nombre;
        this.raza = raza;
        this.color = color;
        this.fechaNacimiento = fechaNacimiento;
        this.pedidree = pedidree;
        this.peso = peso;
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getFechaNacimientoString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNacimiento);
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public boolean isPedidree() {
        return pedidree;
    }

    public void setPedidree(boolean pedidree) {
        this.pedidree = pedidree;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getInfo() {
        String temp = "%s,%s,%s,%s,%s,%f,%s";
        return String.format(temp, nombre, raza, color, getFechaNacimientoString(), pedidree, peso, cedula);
    }

    @Override
    public String toString() {
        return "Mascota{" + "nombre=" + nombre + ", raza=" + raza + ", color=" + color + ", fechaNacimiento=" + fechaNacimiento + ", pedidree=" + pedidree + ", peso=" + peso + ", cedula=" + cedula + '}';
    }

}
