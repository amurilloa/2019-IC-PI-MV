/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyecto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import util.ManejoArchivos;
import util.Util;

/**
 *
 * @author allanmual
 */
public class Logica {

    private ManejoArchivos ma;

    private static final String RUTA_MASC = "datos/mascotas.txt";

    public Logica() {
        ma = new ManejoArchivos();
    }

    public void registrarMascota(Mascota nueva) {
        ma.escribir(RUTA_MASC, nueva.getInfo());
    }

    public Mascota[] consultarMascotas() {
        //Separando el texto del archivo en lineas que representan una mascota
        String[] mascotas = ma.leer(RUTA_MASC).split("\n");

        //Creamos un arreglo de mascotas del mismo tamaño que las lineas del archivo
        Mascota[] arr = new Mascota[mascotas.length];

        //Recorremos linea por linea para poder separar los datos de cada mascota
        for (int i = 0; i < mascotas.length; i++) {
            //Separamos una mascota en datos independientes
            String[] datos = mascotas[i].split(",");

            //Creamos una mascota con los datos 
            Mascota m = new Mascota();
            m.setNombre(datos[0]);
            m.setRaza(datos[1]);
            m.setColor(datos[2]);
            m.setFechaNacimiento(Util.convertirFecha(datos[3]));
            m.setPedidree(Boolean.parseBoolean(datos[4]));
            m.setPeso(Double.parseDouble(datos[5]));
            m.setCedula(datos[6]);

            //Agregamos la mascota al arreglo
            arr[i] = m;
        }
        return arr;
    }

    public Mascota[] consultarMascotas(String cedula) {
        //Separando el texto del archivo en lineas que representan una mascota
        String[] mascotas = ma.leer(RUTA_MASC).split("\n");

        //Creamos un arreglo de mascotas del mismo tamaño que las lineas del archivo
        Mascota[] arr = new Mascota[mascotas.length];

        //Recorremos linea por linea para poder separar los datos de cada mascota
        int i = 0;
        for (String linea : mascotas) {
            //Separamos una mascota en datos independientes
            String[] datos = linea.split(",");
            if (cedula.equals(datos[6])) {
                //Creamos una mascota con los datos 
                Mascota m = new Mascota();
                m.setNombre(datos[0]);
                m.setRaza(datos[1]);
                m.setColor(datos[2]);
                m.setFechaNacimiento(Util.convertirFecha(datos[3]));
                m.setPedidree(Boolean.parseBoolean(datos[4]));
                m.setPeso(Double.parseDouble(datos[5]));
                m.setCedula(datos[6]);
                //Agregamos la mascota al arreglo
                arr[i++] = m;
            }
        }
        return arr;
    }

    public String impMascotas(Mascota[] mascotas) {
        String msj = "";
        String formato = "%d. %s : %s, %s, %.0fkg, %d años, %s\n";

        for (int i = 0; i < mascotas.length; i++) {
            Mascota m = mascotas[i];
            if (m == null) {
                return msj;
            }
            String ped = m.isPedidree() ? "Pedigree" : "Sin Pedigree";
            int edad = calcEdad(m.getFechaNacimientoString());

            msj += String.format(formato, (i + 1),
                    m.getNombre(), m.getRaza(), ped,
                    m.getPeso(), edad, m.getColor());
        }
        return msj;
    }

    private int calcEdad(String fecha) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecNan = LocalDate.parse(fecha, dtf);
        LocalDate hoy = LocalDate.now();

        Period periodo = Period.between(fecNan, hoy);
//        System.out.printf("%d años, %d meses, %d días",
//                periodo.getYears(), periodo.getMonths(), periodo.getDays());

        return periodo.getYears();
    }

    public void editarMascota(String info, Mascota temp) {
        String datos = ma.leer(RUTA_MASC);
        datos = datos.replaceAll(info, temp.getInfo()).trim();
        ma.escribir(RUTA_MASC, datos, false);
    }

    public void eliminarMascota(Mascota mascota) {
        String datos = ma.leer(RUTA_MASC);
        datos = datos.replaceAll(mascota.getInfo()+"\n", "").trim();
        ma.escribir(RUTA_MASC, datos, false);
    }

}
