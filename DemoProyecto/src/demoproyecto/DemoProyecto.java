/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoproyecto;

import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import util.Util;

/**
 *
 * @author allanmual
 */
public class DemoProyecto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Date fecha1 = new Date("2010/12/20");
        Mascota m = new Mascota("Capitan", "Pastor", "Cafe", fecha1,
                true, 45.23, "206470762");
        //System.out.println(m.getInfo());

//        String t1 = "Capitán,Pastor Alemán,Negro,28/06/2016,true,43.000000,206470762\n"
//                + "Max,Pastor Alemán,Cafe,10/12/2014,false,45.000000,207830110\n"
//                + "Firulais,Doberman,Cafe,12/12/2012,true,45.000000,206470762\n"
//                + "";
//
//        String[] t2 = {"Capitán,Pastor Alemán,Negro,28/06/2016,true,43.000000,206470762",
//            "Max,Pastor Alemán,Cafe,10/12/2014,false,45.000000,207830110",
//            "Capitán,Pastor Alemán,Negro,28/06/2016,true,43.000000,206470762"};
//
//        t2 = t1.split("\n");
//        System.out.println(t2[0]);
//        String[] t3 = t2[0].split(",");
//        System.out.println(t3[0]);
//        System.out.println(t3[3]);
//        System.out.println(temp[0]);
        Logica log = new Logica();

        String menu = "Veterinaria - UTN v0.1\n"
                + "1. Mantenimiento Clientes\n"
                + "2. Mantenimiento Mascotas\n"
                + "3. Consulta Veterinaria\n"
                + "4. Salir";

        String mMascotas = "Veterinaria - UTN v0.1\n"
                + "1. Consultar máscotas\n"
                + "2. Ingresar mascota\n"
                + "3. Modificar una mascota\n"
                + "4. Eliminar una mascota\n"
                + "5. Volver";

        String salir = "¿Está seguro que desea salir?";

        APP:
        while (true) {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    //TODO: Mantenimiento de clientes
                    break;
                case 2:
                    MASC:
                    while (true) {
                        op = Util.leerInt(mMascotas);
                        switch (op) {
                            case 1:
                                Mascota[] arr = log.consultarMascotas();
                                Util.mostrar(log.impMascotas(arr));
                                break;
                            case 2:
                                //Registro de mascotas
                                String nombre = Util.leerTexto("Nombre");
                                String raza = Util.leerTexto("Raza");
                                String color = Util.leerTexto("Color");
                                Date fecha = Util.leerFecha("Fecha Nacimiento");
                                boolean pedigree = Util.confirmar("¿Pedigree?");
                                double peso = Util.leerDouble("Peso(kg)");
                                String cedula = Util.leerTexto("Cédula");

                                Mascota temp = new Mascota(nombre, raza, color,
                                        fecha, pedigree, peso, cedula);
                                log.registrarMascota(temp);
                                break;
                            case 3:
                                //TODO: Luis: Modifcar mascota
                                String ced = Util.leerTexto("Digite la cédula del dueño");
                                Mascota[] arrCli = log.consultarMascotas(ced);

                                int num = Util.leerInt("Mascotas:\n"
                                        + log.impMascotas(arrCli)
                                        + "\nSeleccione una mascota");

                                Mascota mEdi = arrCli[num - 1];
                                String info = mEdi.getInfo();
                                nombre = Util.leerTexto("Nombre", mEdi.getNombre());
                                raza = Util.leerTexto("Raza", mEdi.getRaza());
                                color = Util.leerTexto("Color", mEdi.getColor());
                                fecha = Util.leerFecha("Fecha Nacimiento", mEdi.getFechaNacimientoString());
                                String ped = m.isPedidree() ? ">> Con Pedigree" : ">> Sin Pedigree";
                                pedigree = Util.confirmar(ped + "\n¿Pedigree?");
                                peso = Util.leerDouble("Peso(kg)", mEdi.getPeso());
                                cedula = Util.leerTexto("Cédula", mEdi.getCedula());
                                ced = cedula;
                                temp = new Mascota(nombre, raza, color,
                                        fecha, pedigree, peso, cedula);
                                log.editarMascota(info, temp);
                                arrCli = log.consultarMascotas(ced);
                                Util.mostrar(log.impMascotas(arrCli));
                                break;
                            case 4:
                                ced = Util.leerTexto("Digite la cédula del dueño(0:todas)");
                                arrCli = "0".equals(ced) ? log.consultarMascotas() : log.consultarMascotas(ced);
                                num = Util.leerInt("Mascotas:\n"
                                        + log.impMascotas(arrCli)
                                        + "\nSeleccione una mascota");
                                Mascota mEli = arrCli[num - 1];
                                if (Util.confirmar("¿Está seguro que desea eliminar a: " + mEli.getNombre())) {
                                    log.eliminarMascota(mEli);
                                }
                                break;
                            case 5:
                                break MASC;
                            default:
                                Util.mostrar("Opción Inválida!!");
                        }
                    }
                    break;
                case 3:
                    break;
                case 4:
                    if (Util.confirmar(salir)) {
                        break APP;
                    } else {
                        break;
                    }
                default:
                    Util.mostrar("Opción Inválida!!");
            }
        }
    }

}
