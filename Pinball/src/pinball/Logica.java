/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Logica {

    private final int ancho;
    private final int alto;
    private final LinkedList<Bola> bolitas;
    private final Tirador tirador;

    public Logica(int ancho, int alto) {
        this.ancho = ancho;
        this.alto = alto;
        bolitas = new LinkedList<>();
        bolitas.add(new Bola(450, 480)); //Prueba
        tirador = new Tirador();
    }

    public void run(Graphics2D g) {
        if (tirador.isPresionado()) {
            tirador.inc(20);
        }
        for (Bola bolita : bolitas) {
            bolita.paint(g);
        }
    }

    public void accionesPre(int tecla) {
        if (tecla == KeyEvent.VK_DOWN) {
            tirador.setPresionado(true);
        }
    }

    public void accionesRel(int tecla) {
        if (tecla == KeyEvent.VK_DOWN) {
            tirador.setPresionado(false);
            
            tirador.setMs(0);
        }

    }

}
