/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.util.Date;

/**
 *
 * @author allanmual
 */
public class Tirador {
    
    private int ms;
    private boolean presionado;

    
    public void inc(int ms){
        this.ms += ms;
    }
   
    public boolean isPresionado() {
        return presionado;
    }

    public void setPresionado(boolean presionado) {
        this.presionado = presionado;
    }

    public void setMs(int ms) {
        this.ms = ms;
    }

    public int getMs() {
        return ms;
    }

}
