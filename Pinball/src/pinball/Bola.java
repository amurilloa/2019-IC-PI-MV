/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinball;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author allanmual
 */
public class Bola {
    
    private int x;
    private int y;
    
    private final Color COLOR = Color.WHITE;
    private final int SIZE = 20;

    public Bola(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.GRAY);
        g.fillOval(x+1, y+1, SIZE, SIZE);
        g.setColor(COLOR);
        g.fillOval(x, y, SIZE, SIZE);
    }
    
    
}
