/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtrasnform;

import com.apple.laf.AquaCaret;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MyPanel extends JPanel {

    Carro carro;

    private double angle;

    public MyPanel() {
        init();
        carro = new Carro(120, 120);
    }

    private void init() {
        setBackground(Color.WHITE);
        setFocusable(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 600);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        int y = getHeight() / 2;//270
        int x = getWidth() / 2;//300

        //Centros
        g.setColor(Color.RED);
        g.drawLine(0, y, getWidth(), y);
        g.drawLine(x, 0, x, getHeight());

        // Cuadricula
        g.setColor(new Color(230, 230, 231));
        x = 0;
        y = 0;
        for (int i = 0; i <= 15; i++) {
            g.drawLine(0, y, getWidth(), y);
            g.drawLine(x, 0, x, getHeight());
            x += 40;
            y += 40;
        }
//        
//        double curAngle = 0.1;
//        for (int j = 0; j < 14; j++) {
//            g.translate(10, 10);
//            g.rotate(Math.toRadians(curAngle));
//            carro.paint(g);
//            curAngle+=0.1;
//        }
//        pi
        //for (int i = 0; i < 10; i++) {
        carro.paint(g);
        
        g.translate(290, 0);
        g.rotate(Math.PI / 4);
        carro.paint(g);
        g.translate(290, 0);
        g.rotate(Math.PI / 4);
        carro.paint(g);
        
        g.rotate(-Math.PI / 4); 
        g.translate(-290,0);
        g.rotate(-Math.PI / 4); 
        g.translate(-290,0);
        g.fillRect(0, 0, 10, 20);
        
        //}
    }
}
