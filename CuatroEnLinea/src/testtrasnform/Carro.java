/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtrasnform;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author allanmual
 */
public class Carro {

    private int x;
    private int y;
    private int dir;

    public Carro(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.BLUE);
        g.drawRoundRect(x, y, 100, 50, 10, 10);
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    

}
