/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author allanmual
 */
public class Hole {

    private final int x;
    private final int y;
    private final Color FRONT_COL = Color.DARK_GRAY;
    private final Color BACKG_COL = Color.WHITE;
    private boolean chip;
    private int player;

    public static final int SIZE = 80;

    public Hole(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g) {
        g.setColor(FRONT_COL);
        g.fillRect(x, y, SIZE, SIZE);
        g.setColor(BACKG_COL);
        g.fillOval(x + 2, y + 2, SIZE - 4, SIZE - 4);
//        g.setColor(Color.CYAN);
//        g.drawRect(x + (SIZE / 2 - 1), y + (SIZE / 2 - 1), 2, 2);
    }

    public boolean isChip() {
        return chip;
    }

    public void setChip(boolean chip) {
        this.chip = chip;
    }

    public Rectangle getBounds() {
        return new Rectangle(x + (SIZE / 2 - 1), y + (SIZE / 2 - 1), 2, 2);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getPlayer() {
        return player;
    }
}
