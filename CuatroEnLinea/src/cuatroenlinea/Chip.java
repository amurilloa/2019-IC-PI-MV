/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author allanmual
 */
public class Chip {

    private int x;
    private int y;
    private int dir;
    private Color color;
    private final int column;
    private final int player;

    private final int SIZE = 76;

    public Chip(int x, int y, int column, int player) {
        dir = 1;
        color = Color.DARK_GRAY;
        this.x = x;
        this.y = y;
        this.column = column;
        this.player = player;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, SIZE, SIZE);
        
        //Para ver el bounds
//        g.setColor(Color.CYAN);
//        g.drawRect(x + (SIZE / 2 - 1), y + (SIZE / 2 - 1), 2, 2);
    }

    public void move() {
        switch (dir) {
            case 0:
                break;
            case 1:
                y += 3;
                break;
        }
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getDir() {
        return dir;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Rectangle getBounds() {
        return new Rectangle(x + (SIZE / 2 - 1), y + (SIZE / 2 - 1), 2, 2);
    }

    public void bounce(Hole[][] holes) {
        for (int f = 0; f < holes.length; f++) {
            Hole holeAux = holes[f][column];
            if (getBounds().intersects(holeAux.getBounds())) {
                if ((f == holes.length - 1) || (holes[f + 1][column].isChip())) {
                    holeAux.setChip(true);
                    dir = 0;
                    x = holeAux.getX()+2;
                    y = holeAux.getY()+2;
                    holeAux.setPlayer(player);
                }
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }

    public int getPlayer() {
        return player;
    }
    
}
