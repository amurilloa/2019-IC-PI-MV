/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Logic {

    private boolean pause;
    private boolean start;
    private boolean exit;
    private boolean color;

    private final int x;
    private final int y;

    private final Hole[][] holes;
    private final LinkedList<Chip> chips;

    private int player;

    public Logic(int width, int height) {
        x = width / 2;
        y = height / 2;
        chips = new LinkedList<>();
        holes = new Hole[6][7];
        initHoles();
        player = 1;
    }

    private void initHoles() {
        //Se crean los huecos de todo el tablero, se les da el x,y inicial de cada uno 
        final int START = 20;
        int auxX = START;
        int auxY = 60;

        for (int f = 0; f < holes.length; f++) {
            for (int c = 0; c < holes[f].length; c++) {
                holes[f][c] = new Hole(auxX, auxY);
                auxX += Hole.SIZE;
            }
            auxY += Hole.SIZE;
            auxX = START;
        }
    }

    public void run(Graphics g) {
        if (exit) {
            g.setFont(new Font("Fuente 1", Font.BOLD, 40));
            g.setColor(Color.BLACK);
            g.drawString("Y: YES | N: NO", x - 143, y + 15);
        } else if (start && !pause) {
            help(g);
            for (int f = 0; f < holes.length; f++) {
                for (int c = 0; c < holes[f].length; c++) {
                    holes[f][c].paint(g);
                }
            }
            for (Chip cir : chips) {
                cir.paint(g);
                cir.move();
                cir.bounce(holes);
            }
            if (won(player)) {
                System.out.println("Ganó el jugador: " + player);
                player = 1;
                start = false;
                pause = false;
                color = false;
                chips.clear();
                initHoles();
            }

        } else if (pause) {
            g.setFont(new Font("Fuente 3", Font.BOLD, 40));
            g.setColor(Color.BLACK);
            g.drawString("PAUSE", x - 68, y + 15);
        } else {
            g.setFont(new Font("Fuente 4", Font.BOLD, 40));
            g.setColor(Color.BLACK);
            g.drawString("PRESS SPACE FOR START", x - 255, y + 15);
        }

        g.setFont(new Font("Fuente 5", Font.BOLD, 13));
        g.setColor(Color.GRAY);
        g.drawString("X: EXIT", x + 233, y - 265);

    }

    public void actions(int keyCode) {
        if (keyCode == KeyEvent.VK_SPACE && !start) {
            start = true;
        } else if (keyCode == KeyEvent.VK_S) {
            player = 1;
            start = false;
            pause = false;
            color = false;
            chips.clear();
            initHoles();
        } else if (keyCode == KeyEvent.VK_P && start) {
            pause = !pause;
        } else if (keyCode == KeyEvent.VK_X) {
            exit = true;
        } else if (exit && keyCode == KeyEvent.VK_Y) {
            System.exit(0);
        } else if (exit && keyCode == KeyEvent.VK_N) {
            exit = false;
        } else if (start && !pause) {
            if (keyCode >= 49 && keyCode <= 55) {
                dropChip(keyCode - 49);
            }
        }
    }

    private void help(Graphics g) {
        g.setFont(new Font("Fuente 2", Font.BOLD, 14));
        g.setColor(Color.GRAY);
        int auxY = 35;
        int auxX = 55;
        for (int i = 1; i <= 7; i++) {
            g.drawString(String.valueOf(i), auxX, auxY);
            g.drawString("⇣", auxX, auxY + 15);
            auxX += 80;
        }

    }

    private void dropChip(int numCol) {
        //Si la ultima que se agregó ya no se está moviendo, podemos agregar otra
        if ((chips.isEmpty() || chips.getLast().getDir() == 0) && !holes[0][numCol].isChip()) {
            //Se calcula el x a partir del número de la columna
            int auxX = 22 + (numCol * 80);
            int auxY = -80; //Negativo para que el movimiento empiece fuera de la ventana 
            player = color ? 1 : 2;
            Chip chip = new Chip(auxX, auxY, numCol, player);
            chip.setColor(color ? Color.BLUE : Color.RED); //Se cambia el color con respecto a la última ficha
            color = !color; // Se invierte la variable de color
            chips.add(chip); //Se agrega a la lista para que se pinte, mueva, choque, etc. 

        }
    }

    /**
     * Revisa sin un jugador ganó horizontal, vertical o diagonal
     *
     * @param player numero de jugador 1-2
     * @return true si alguien ganó
     */
    private boolean won(int player) {
        // horizontalCheck 
        for (int j = 0; j < holes[0].length - 3; j++) {
            for (int i = 0; i < holes.length; i++) {
                if (holes[i][j].getPlayer() == player && holes[i][j + 1].getPlayer() == player && this.holes[i][j + 2].getPlayer() == player && holes[i][j + 3].getPlayer() == player) {
                    return true;
                }
            }
        }
        
        // verticalCheck
        for (int i = 0; i < holes.length - 3; i++) {
            for (int j = 0; j < holes[i].length; j++) {
                if (holes[i][j].getPlayer() == player && holes[i + 1][j].getPlayer() == player && holes[i + 2][j].getPlayer() == player && holes[i + 3][j].getPlayer() == player) {
                    return true;
                }
            }
        }

        // ascendingDiagonalCheck 
        for (int i = 3; i < holes.length; i++) {
            for (int j = 0; j < holes[i].length - 3; j++) {
                if (holes[i][j].getPlayer() == player && holes[i - 1][j + 1].getPlayer() == player && holes[i - 2][j + 2].getPlayer() == player && holes[i - 3][j + 3].getPlayer() == player) {
                    return true;
                }
            }
        }
        
        // descendingDiagonalCheck
        for (int i = 3; i < holes.length; i++) {
            for (int j = 3; j < holes[i].length; j++) {
                if (holes[i][j].getPlayer() == player && holes[i - 1][j - 1].getPlayer() == player && holes[i - 2][j - 2].getPlayer() == player && holes[i - 3][j - 3].getPlayer() == player) {
                    return true;
                }
            }
        }
        
        return false;
    }
}
